package uz.pdp;


import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;
import uz.pdp.bot.BotService;
import uz.pdp.bot.HelperService;
import uz.pdp.bot.FoodLongPollingBot;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Properties;

public class Main {

    public static void main(String[] args) throws TelegramApiException {

        telegramStart();

        new HelperService();
    }

    private static void telegramStart() throws TelegramApiException {
        Properties properties = new Properties();

        try (FileInputStream fileOutputStream = new FileInputStream("src/main/resources/application.properties")) {
            properties.load(fileOutputStream);
        } catch (Exception e) {
            System.exit(10);
        }

        TelegramBotsApi botsApi = new TelegramBotsApi(DefaultBotSession.class);
        FoodLongPollingBot foodLongPollingBot = new FoodLongPollingBot(
                properties,
                new BotService(properties));

        botsApi.registerBot(foodLongPollingBot);
    }

}