package uz.pdp.enums;

public enum OrderStatus {
    DRAFT,
    NEW,
    CANCELLED,
    COOKING,
    SEND,
    RECEIVED,
    REJECTED
}
