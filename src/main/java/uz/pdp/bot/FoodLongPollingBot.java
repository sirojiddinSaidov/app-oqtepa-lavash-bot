package uz.pdp.bot;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import uz.pdp.enums.StateEnum;
import uz.pdp.model.Category;
import uz.pdp.model.Client;
import uz.pdp.model.Product;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

public class FoodLongPollingBot extends TelegramLongPollingBot {

    private final IBotService botService;

    private final String username;

    public FoodLongPollingBot(Properties properties, IBotService botService) {
        super(properties.getProperty("bot.token"));
        this.username = properties.getProperty("bot.username");
        if (botService == null)
            System.exit(100);
        this.botService = botService;
        ketmon();
    }

    @Override
    public void onUpdateReceived(Update update) {
        try {
            if (update.hasMessage())
                receiveMessage(update);
            else if (update.hasCallbackQuery())
                receiveCallbackData(update);
        } catch (TelegramApiException e) {
            System.out.println("Exception");
        }
    }

    @Override
    public String getBotUsername() {
        return username;
    }

    private void receiveCallbackData(Update update) throws TelegramApiException {
        CallbackQuery callbackQuery = update.getCallbackQuery();
        Long chatId = callbackQuery.getMessage().getChatId();
        Client client = BotService.getClient(chatId);
        String data = callbackQuery.getData();
        if (data.equals(BotConstantText.MAIN_MENU)) {
            execute(botService.deleteMessage(chatId, callbackQuery.getMessage().getMessageId()));
            for (SendMessage sendMessage : botService.mainMenu(chatId))
                execute(sendMessage);
            return;
        }

        switch (client.getState()) {
            case MAIN_MENU -> {
                switch (data) {
                    case BotConstantText.MAKE_ORDER_BUTTON -> {
                        DeleteMessage deleteMessage = botService.deleteMessage(chatId, callbackQuery.getMessage().getMessageId());
                        execute(deleteMessage);
                        execute(botService.showOrderType(chatId, client));
                    }
                    case BotConstantText.MY_ORDERS_BUTTON -> {
                        List<SendMessage> sendMessages = botService.orderHistories(chatId, client);
                        for (SendMessage message : sendMessages) {
                            execute(message);
                        }
                    }
                    case BotConstantText.ABOUT_US_BUTTON -> {
                        execute(botService.deleteMessage(chatId, callbackQuery.getMessage().getMessageId()));
                        execute(botService.showAboutUs(chatId, client));
                    }
                }
            }
            case CHOOSE_CATEGORY_OR_PRODUCT -> {
                execute(botService.deleteMessage(chatId, callbackQuery.getMessage().getMessageId()));
                execute(botService.getCategoryOrProduct(chatId, client, data));
            }
        }
    }

    private void receiveMessage(Update update) throws TelegramApiException {
        Message message = update.getMessage();
        Long chatId = message.getChatId();
        Client client = BotService.getClient(chatId);
        if (message.hasText())
            whenHasText(update, message, client, chatId);
        else if (message.hasLocation()) {
            if (client.getState().equals(StateEnum.SEND_LOCATION)) {
                execute(botService.sentLocation(chatId, client, message.getLocation()));
            }
        } else if (message.hasContact()) {
            if (client.getState().equals(StateEnum.SEND_CONTACT))
                execute(botService.sendContact(message, chatId, client));
        }

    }

    private void onStartText(Update update, Client client, Long chatId) throws TelegramApiException {
        if (client == null)
            execute(botService.initialMessage(update));
        if (client == null || !client.isVerified())
            execute(botService.unverifiedUserChooseLanguage(chatId, update.getMessage().getChat().getFirstName()));
        else {
            List<SendMessage> sendMessages = botService.onStartVerifiedUser(chatId);
            for (SendMessage sendMessage1 : sendMessages)
                execute(sendMessage1);
        }
    }

    private void whenHasText(Update update, Message message, Client client, Long chatId) throws TelegramApiException {
        String text = message.getText();
        if (text.equals("/start")) {
            onStartText(update, client, chatId);
            return;
        } else if (text.equals("⬅️ Main menu")) {
            for (SendMessage sendMessage : botService.mainMenu(chatId))
                execute(sendMessage);
        }

        if (client != null) {
            StateEnum state = client.getState();
            switch (state) {
                case LANGUAGE -> {
                    for (SendMessage sendMessage : botService.chooseLanguage(update))
                        execute(sendMessage);
                }
                case CHOOSE_REGION -> execute(botService.chooseRegion(text, chatId, client));
                case ENTER_SMS_CODE -> {
                    for (SendMessage sendMessage : botService.enterSmsCode(text, chatId, client))
                        execute(sendMessage);
                }
                case SELECT_ORDER_TYPE -> {
                    for (SendMessage sendMessage : botService.selectOrderType(chatId, text, client))
                        execute(sendMessage);
                }
                case SEND_LOCATION -> {
                    SendMessage sendMessage = botService.changeDeliveryType(text, chatId, client);
                    execute(sendMessage);
                }
                case CONFIRMING_LOCATION -> {
                    SendMessage sendMessage = botService.confirmSentLocation(text, chatId, client);
                    execute(sendMessage);
                }
                case ENTERING_NAME -> {
                    SendMessage sendMessage = botService.confirmNameAndSendNearestBranches(text, chatId, client);
                    execute(sendMessage);
                }
                case CHOOSING_BRANCH -> {
                    SendMessage sendMessage = botService.selectBranchAndSendMenu(text, chatId, client);
                    execute(sendMessage);
                }
                case FURTHER_ADDRESS_DETAILS -> {
                    SendMessage sendMessages = botService.conformedOrNotConformedAddress(text, chatId, client);
                    execute(sendMessages);
                }
                case LOCATION_DEFINED -> {
                    List<SendMessage> sendMessages = botService.afterFurtherDetails(text, chatId, client);
                    for (SendMessage sendMessage : sendMessages) {
                        execute(sendMessage);
                    }
                }
            }
        }
    }


    private void ketmon() {
//        if (HelperService.getCategories().stream()
//                .noneMatch(category -> category.getPhoto() != null)) {

        File category = new File("photos\\category");
        for (File file : category.listFiles()) {
            try {
                Message execute = execute(SendPhoto.builder()
                        .chatId(-994632275L)
                        .photo(new InputFile(new FileInputStream(file), file.getName()))
                        .build());
                String name = file.getName().split(".jpg")[0];
                Optional<Category> categoryByName = getCategoryByName(name);
                if (categoryByName.isPresent()) {
                    Category category1 = categoryByName.get();
                    category1.setPhoto(execute.getPhoto().get(execute.getPhoto().size() - 1).getFileId());
                }
            } catch (TelegramApiException e) {
                throw new RuntimeException(e);
            } catch (FileNotFoundException e) {
                throw new RuntimeException(e);
            }

        }

        File products = new File("photos\\product");

        for (File file : products.listFiles()) {
            try {
                Message execute = execute(SendPhoto.builder()
                        .chatId(-994632275L)
                        .photo(new InputFile(new FileInputStream(file), file.getName()))
                        .build());
                String name = file.getName().split(".jpg")[0];
                Optional<Product> productByName = getProductByName(name);
                if (productByName.isPresent()) {
                    Product product = productByName.get();
                    product.setPhoto(execute.getPhoto().get(execute.getPhoto().size() - 1).getFileId());
                }
            } catch (TelegramApiException e) {
                throw new RuntimeException(e);
            } catch (FileNotFoundException e) {
                throw new RuntimeException(e);
            }

        }
    }
//    }

    private Optional<Category> getCategoryByName(String name) {
        return HelperService.getCategories().stream()
                .filter(category -> category.getName().equals(name))
                .findFirst();

    }

    private Optional<Product> getProductByName(String name) {
        return HelperService.getProducts().values().stream()
                .filter(category -> category.getName().equals(name))
                .findFirst();
    }
}
