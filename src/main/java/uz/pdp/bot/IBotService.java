package uz.pdp.bot;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.objects.Location;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import uz.pdp.enums.LanguageEnum;
import uz.pdp.model.Client;

import java.util.List;

public interface IBotService {

    SendMessage initialMessage(Update update);

    List<SendMessage> onStartVerifiedUser(Long chatId);

    List<SendMessage> chooseLanguage(Update update);

    SendMessage unverifiedUserChooseLanguage(Long chatId, String name);

    SendMessage chooseRegion(String region, Long chatId, Client client);

    SendMessage sendContact(Message message, Long chatId, Client client);

    List<SendMessage> enterSmsCode(String text, Long chatId, Client client);

    List<SendMessage> mainMenu(Long chatId);

    DeleteMessage deleteMessage(Long chatId, Integer messageId);

    SendMessage showOrderType(Long chatId, Client client);

    List<SendMessage> selectOrderType(Long chatId, String text, Client client);

    SendMessage changeDeliveryType(String text,Long chatId, Client client);

    SendMessage sentLocation(Long chatId, Client client, Location location);

    SendMessage confirmSentLocation(String text, Long chatId, Client client);

    SendMessage confirmNameAndSendNearestBranches(String text, Long chatId, Client client);

    SendMessage selectBranchAndSendMenu(String text, Long chatId, Client client);

    SendMessage conformedOrNotConformedAddress(String text, Long chatId, Client client);
    List<SendMessage> afterFurtherDetails(String text, Long chatId, Client client);

    List<SendMessage> orderHistories(Long chatId, Client client);

    SendMessage showAboutUs (Long chatId, Client client);

    SendPhoto getCategoryOrProduct(Long chatId, Client client, String data);

    List<SendPhoto> sendPhotoInitials();
}

