package uz.pdp.bot;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.objects.*;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardRemove;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import uz.pdp.enums.LanguageEnum;
import uz.pdp.enums.OrderStatus;
import uz.pdp.enums.OrderTypeEnum;
import uz.pdp.enums.StateEnum;
import uz.pdp.model.*;

import java.io.File;
import java.io.FileInputStream;
import java.util.*;
import java.util.stream.Collectors;


public class BotService implements IBotService {

    private final int smsCodeDigitCount;

    public BotService(Properties properties) {
        this.smsCodeDigitCount = Integer.parseInt(properties.getProperty("smsCodeDigitCount", "4"));
    }

    /**
     * INITIAL MESSAGE TO CLIENT
     *
     * @param update Update
     * @return SendMessage
     */
    @Override
    public SendMessage initialMessage(Update update) {
        Long chatId = update.getMessage().getChatId();
        String firstName = update.getMessage().getChat().getFirstName();

        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        String message = "Assalomu alaykum " + firstName + ". Men Oqtepa Lavash yetkazib berish xizmati botiman!\n" +
                "Привет " + firstName + "! Я бот службы доставки Oqtepa Lavash!\n" +
                "Hi " + firstName + "! I am Oqtepa Lavash delivery service bot!";
        sendMessage.setText(message);
        return sendMessage;
    }

    /**
     * THIS METHOD MAKE SEND MESSAGE WHEN CLIENT TYPED /start TEXT
     *
     * @param chatId Long
     * @return List<SendMessage></SendMessage>
     */
    @Override
    public List<SendMessage> onStartVerifiedUser(Long chatId) {
        return List.of(mainMenuFirstMessage(chatId),
                mainMenu(chatId, "Quyidagilardan birini tanlang"));
    }

    @Override
    public List<SendMessage> chooseLanguage(Update update) {
        String text = update.getMessage().getText();
        Long chatId = update.getMessage().getChatId();
        Client client = getClient(chatId);
        LanguageEnum language = switch (text) {
            case BotConstantText.UZ -> LanguageEnum.UZ;
            case BotConstantText.RU -> LanguageEnum.RU;
            case BotConstantText.EN -> LanguageEnum.EN;
            default -> null;
        };
        if (language == null) {
            if (client == null || !client.isVerified())
                return List.of(unverifiedUserChooseLanguage(chatId, update.getMessage().getChat().getFirstName()));
            else onStartVerifiedUser(chatId);
        }

        client.setLanguage(language);
        SendMessage sendMessage = makeSendMessageWithRegion(chatId, client);
        client.setState(StateEnum.CHOOSE_REGION);
        return List.of(sendMessage);
    }

    @Override
    public SendMessage unverifiedUserChooseLanguage(Long chatId, String name) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        sendMessage.setText("""
                Muloqot tilini tanlang
                Выберите язык
                Select Language""");

        KeyboardRow first = new KeyboardRow(List.of(new KeyboardButton(BotConstantText.UZ)));

        KeyboardRow second = new KeyboardRow(List.of(new KeyboardButton(BotConstantText.RU)));

        KeyboardRow third = new KeyboardRow(List.of(new KeyboardButton(BotConstantText.EN)));

        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup(List.of(first, second, third));
        replyKeyboardMarkup.setResizeKeyboard(true);
        sendMessage.setReplyMarkup(replyKeyboardMarkup);
        addClient(chatId, name);
        Client client = getClient(chatId);
        client.setState(StateEnum.LANGUAGE);
        return sendMessage;
    }

    @Override
    public SendMessage chooseRegion(String region, Long chatId, Client client) {
        LanguageEnum language = client.getLanguage();
        Optional<Region> optionalRegion = HelperService.getRegions().stream().filter(oneRegion -> oneRegion.name().equals(region)).findFirst();
        if (optionalRegion.isEmpty())
            return makeSendMessageWithRegion(chatId, client);

        client.setRegion(optionalRegion.get());
        return makeSendContactSendMessage(chatId, language, client);
    }

    @Override
    public SendMessage sendContact(Message message, Long chatId, Client client) {
        Contact contact = message.getContact();
        if (!chatId.equals(contact.getUserId()))
            return chooseRegion(
                    client.getRegion().name(),
                    chatId,
                    client);
        String phoneNumber = contact.getPhoneNumber();
        client.setPhone(phoneNumber);
        String randomCode = getRandomCode(smsCodeDigitCount);
        String text = makeSmsCodeMessage(client, randomCode);
        //todo third party sms service
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup(
                List.of(new KeyboardRow(List.of(new KeyboardButton(
                        getMessageByLanguageAndKey("notGetSmsCodeButton", client.getLanguage())
                ))))
        );
        replyKeyboardMarkup.setResizeKeyboard(true);
        client.setState(StateEnum.ENTER_SMS_CODE);
        client.setSMSCode(randomCode);
        return SendMessage.builder()
                .chatId(chatId)
                .text(text)
                .parseMode("HTML")
                .replyMarkup(replyKeyboardMarkup)
                .build();
    }

    @Override
    public List<SendMessage> enterSmsCode(String text, Long chatId, Client client) {
        LanguageEnum language = client.getLanguage();
        String notGetSmsCodeButton = getMessageByLanguageAndKey("notGetSmsCodeButton", client.getLanguage());

        if (notGetSmsCodeButton.equals(text))
            return List.of(makeSendContactSendMessage(chatId, language, client));

        if (!text.equals(client.getSMSCode())) {
            client.decrementAttemptCount();
            if (client.getAttemptCount() < 1)
                return List.of(makeSendContactSendMessage(chatId, language, client));

            return List.of(SendMessage.builder()
                    .chatId(chatId)
                    .text(getMessageByLanguageAndKey(
                            "incorrectSmsCode",
                            language))
                    .replyMarkup(new ReplyKeyboardRemove(true))
                    .build());
        }

        SendMessage firstMessage = SendMessage.builder()
                .chatId(chatId)
                .text("Registratsiya jarayonidan muvaffaqiyatli o'tdingiz!")
                .replyMarkup(mainMenuButton())
                .build();

        client.setVerified(true);

        return List.of(firstMessage,
                mainMenu(chatId, getMessageByLanguageAndKey("mainMenuWithMenu", language)));
    }

    @Override
    public List<SendMessage> mainMenu(Long chatId) {
        SendMessage firstMessage = mainMenuFirstMessage(chatId);
        Client client = getClient(chatId);
//        if (getLastOrder(chatId).getStatus().equals(OrderStatus.DRAFT)) {
//            SendMessage secondMessage = mainMenuFromFoodMenu(chatId, getMessageByLanguageAndKey("mainMenuWithMenu", client.getLanguage()));
//            return List.of(firstMessage, secondMessage);
//        } else {
        SendMessage secondMessage = mainMenu(chatId, getMessageByLanguageAndKey("mainMenuWithMenu", client.getLanguage()));
        client.setState(StateEnum.MAIN_MENU);
        return List.of(firstMessage, secondMessage);
//        }
    }

    @Override
    public DeleteMessage deleteMessage(Long chatId, Integer messageId) {
        return DeleteMessage.builder()
                .chatId(chatId)
                .messageId(messageId)
                .build();
    }

    @Override
    public SendMessage showOrderType(Long chatId, Client client) {
        client.setState(StateEnum.SELECT_ORDER_TYPE);
        return SendMessage.builder()
                .chatId(chatId)
                .text(getMessageByLanguageAndKey("selectOrderType", client.getLanguage()))
                .replyMarkup(makeOrderTypeButtons(client.getLanguage()))
                .build();
    }

    @Override
    public List<SendMessage> selectOrderType(Long chatId, String text, Client client) {
        OrderTypeEnum orderType;
        if (text.equals(getMessageByLanguageAndKey("deliveryOrderType", client.getLanguage()))) {
            orderType = OrderTypeEnum.DELIVERY;
        } else if (text.equals(getMessageByLanguageAndKey("selfOrderType", client.getLanguage()))) {
            orderType = OrderTypeEnum.SELF;
        } else if (text.equals(getMessageByLanguageAndKey("backButton", client.getLanguage())))
            return mainMenu(chatId);
        else
            return List.of(showOrderType(chatId, client));

        Order order = getLastOrder(chatId);
        order.setOrderType(orderType);
        order.setClient(client);
        client.setState(StateEnum.SEND_LOCATION);
        return List.of(makeSendLocationTextAndLocationButton(chatId, client));
    }

    @Override
    public SendMessage showAboutUs(Long chatId, Client client) {
        client.setState(StateEnum.SEND_ABOUT_US);
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        sendMessage.setParseMode("HTML");
        sendMessage.setText(getMessageByLanguageAndKey("aboutUsText", client.getLanguage()));
        sendMessage.setReplyMarkup(makeMainInlineButton(client.getLanguage()));
        client.setState(StateEnum.MAIN_MENU);
        return sendMessage;
    }

    @Override
    public SendMessage changeDeliveryType(String text, Long chatId, Client client) {
        if (text.equals(getMessageByLanguageAndKey("backButton", client.getLanguage())))
            return showOrderType(chatId, client);
        return makeSendLocationTextAndLocationButton(chatId, client);
    }

    @Override
    public SendMessage sentLocation(Long chatId, Client client, Location location) {
        Order order = getLastOrder(chatId);
        order.setLat(location.getLatitude());
        order.setLon(location.getLongitude());
        //todo 3party api use
        String apiRes = "Ташкент, улица Беруни, 3А";
        client.setState(StateEnum.CONFIRMING_LOCATION);
        client.setState(StateEnum.FURTHER_ADDRESS_DETAILS);
        return SendMessage.builder()
                .chatId(chatId)
                .text(getMessageByLanguageAndKey("confirmYourAddress", client.getLanguage()).formatted(apiRes))
                .replyMarkup(makeConfirmLocationButtons(client.getLanguage()))
                .build();
    }

    @Override
    public SendMessage confirmSentLocation(String text, Long chatId, Client client) {
        if (text.equals(getMessageByLanguageAndKey("yesButton", client.getLanguage()))) {
            if (getLastOrder(chatId).getOrderType().equals(OrderTypeEnum.SELF)) {
                return sendEnterNameButtonMessage(chatId, client);
            } else if (getLastOrder(chatId).getOrderType().equals(OrderTypeEnum.DELIVERY)) {
                //NOT MY PROBLEM
            }
        }
        client.setState(StateEnum.SEND_LOCATION);
        return makeSendLocationTextAndLocationButton(chatId, client);
    }

    @Override
    public SendMessage confirmNameAndSendNearestBranches(String text, Long chatId, Client client) {
        if (text.equals(getMessageByLanguageAndKey("backButton", client.getLanguage()))) {
            client.setState(StateEnum.SEND_LOCATION);
            return makeSendLocationTextAndLocationButton(chatId, client);
        }
        client.setName(text);
        client.setState(StateEnum.CHOOSING_BRANCH);

        List<Restaurant> restaurants = nearestRestaurants(client, getLastOrder(chatId));
        if (!restaurants.isEmpty()) {
            return sendNearestBranchesMessage(chatId, client, restaurants);
        }
        return SendMessage.builder()
                .chatId(chatId)
                .text("Nothing")
                .build();
    }

    @Override
    public SendMessage selectBranchAndSendMenu(String text, Long chatId, Client client) {
        if (text.equals(getMessageByLanguageAndKey("backButton", client.getLanguage()))) {
            client.setState(StateEnum.ENTERING_NAME);
            return sendEnterNameButtonMessage(chatId, client);
        } else if (text.equals(getMessageByLanguageAndKey("otherBranchesButton", client.getLanguage()))) {
            return sendAllBranchesMessage(chatId, client);
        }
        List<Restaurant> restaurants = nearestRestaurants(client, getLastOrder(chatId));
        Optional<Restaurant> first = getBranchThatEqualsToText(text, restaurants);
        if (first.isPresent()) {
            getLastOrder(chatId).setBranch(first.get());
            client.setState(StateEnum.ROOT_CATEGORY);

            //NOT MY PROBLEM

            return null;
        } else {
            return sendNearestBranchesMessage(chatId, client, restaurants);
        }
    }

    @Override
    public List<SendMessage> afterFurtherDetails(String text, Long chatId, Client client) {
        SendMessage firstMessage = SendMessage.builder()
                .chatId(chatId)
                .text(getMessageByLanguageAndKey("firstMessageOfOrdering", client.getLanguage()))
                .replyMarkup(mainMenuButton())
                .build();
        client.setState(StateEnum.ROOT_CATEGORY);
        return List.of(
                firstMessage,
                rootCategory(chatId));
    }

    @Override
    public SendMessage conformedOrNotConformedAddress(String text, Long chatId, Client client) {
        if (text.equals(getMessageByLanguageAndKey("yesButton", client.getLanguage()))) {
            SendMessage sendMessage = new SendMessage();
            sendMessage.setChatId(chatId);
            sendMessage.setText(getMessageByLanguageAndKey("furtherDetails", client.getLanguage()));
            client.setState(StateEnum.LOCATION_DEFINED);
            return sendMessage;
        } else {
            client.setState(StateEnum.SEND_LOCATION);
            return makeSendLocationTextAndLocationButton(chatId, client);
        }
    }

    @Override
    public List<SendMessage> orderHistories(Long chatId, Client client) {
        List<Order> orders = HelperService.getOrders().get(chatId);
        if (orders == null || orders.isEmpty())
            return List.of(
                    SendMessage.builder()
                            .chatId(chatId)
                            .text(getMessageByLanguageAndKey("noOrdersYet", client.getLanguage()))
                            .build()
            );

        return orders.stream().map(order -> SendMessage.builder()
                .chatId(chatId)
                .text(oneOrderToTextFormat(order))
                .build()).collect(Collectors.toList());
    }

    @Override
    public SendPhoto getCategoryOrProduct(Long chatId, Client client, String data) {
        if (data.startsWith("Product")) {
            return makeOneProduct(chatId, client, data.split("#")[1],1);
        }

//          Product#asd
//          Category#10
        data = data.split("#")[1];
        Optional<Category> optionalCategory = getCategoryById(data);
        if (optionalCategory.isEmpty())
            return null;

        List<Category> children = getCategoriesByParent(optionalCategory.get());
        List<Product> products = getProductsByCategory(optionalCategory.get().getId());


        return SendPhoto.builder()
                .chatId(chatId)
                .replyMarkup(inlinesForProducts(children, products))
                .photo(new InputFile(optionalCategory.get().getPhoto()))
                .caption(optionalCategory.get().getName())
                .build();
    }

    private SendPhoto makeOneProduct(Long chatId, Client client, String productId,int count) {
        Product product = HelperService.getProducts().get(productId);
        List<List<InlineKeyboardButton>> rows=new ArrayList<>(3);
        rows.add(List.of(
                InlineKeyboardButton.builder()
                .text("-")
                .callbackData(productId+"#"+(count-1))
                .build(),
                InlineKeyboardButton.builder()
                        .text(String.valueOf(count<2?1:count))
                        .callbackData(productId+"#count")
                        .build(),
                InlineKeyboardButton.builder()
                        .text("+")
                        .callbackData(productId+"#"+(count+1))
                        .build()

        ));
        rows.add(List.of(InlineKeyboardButton.builder()
                .text("Savatga qo`shish")
                .callbackData(BotConstantText.ADD_BASKET+"#"+productId)
                .build()
        ));
        rows.add(List.of(InlineKeyboardButton.builder()
                .text("Ortga")
                .callbackData(BotConstantText.BACK+"#"+productId)
                .build()
        ));
        return SendPhoto.builder()
                .chatId(chatId)
                .photo(new InputFile(product.getPhoto()))
                .caption(getProductInfo(product,client.getLanguage()))
                .replyMarkup(InlineKeyboardMarkup.builder().keyboard(rows).build()).build();
    }
    private String getProductInfo(Product product,LanguageEnum language){
        return product.getName() + "\n" +
                getMessageByLanguageAndKey("price", language) + product.getPrice() + "\n" +
                getMessageByLanguageAndKey("description", language) + product.getDescription();
    }

    private InlineKeyboardMarkup inlinesForProducts(List<Category> categories, List<Product> products) {

        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        Iterator<Category> categoryIterator = categories.iterator();

        List<InlineKeyboardButton> row = new ArrayList<>(2);

        while (categoryIterator.hasNext()) {

            Category category = categoryIterator.next();
            row.add(
                    InlineKeyboardButton.builder()
                            .text((category.getEmoji() == null ? "" : category.getEmoji()) + category.getName())
                            .callbackData("Category#" + category.getId())
                            .build());
            if (row.size() == 2) {
                rows.add(row);
                row = new ArrayList<>();
            }
        }
        Iterator<Product> productIterator = products.iterator();
        while (productIterator.hasNext()) {
            Product product = productIterator.next();
            row.add(
                    InlineKeyboardButton.builder()
                            .text(product.getName())
                            .callbackData("Product#" + product.getId().toString())
                            .build());
            if (row.size() == 2) {
                rows.add(row);
                row = new ArrayList<>();
            }
        }

        if (!row.isEmpty())
            rows.add(row);

        return InlineKeyboardMarkup.builder()
                .keyboard(rows)
                .build();

    }


    private SendMessage makeSendLocationTextAndLocationButton(Long chatId, Client client) {
        return SendMessage.builder()
                .chatId(chatId)
                .text(getMessageByLanguageAndKey("sendLocationOrSelectOrder", client.getLanguage()))
                .replyMarkup(makeSendLocation(client.getLanguage()))
                .build();
    }

    private SendMessage mainMenuFirstMessage(Long chatId) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        sendMessage.setText(getMessageByLanguageAndKey("letOrder", getClient(chatId).getLanguage()));
        sendMessage.setReplyMarkup(mainMenuButton());
        getClient(chatId).setState(StateEnum.MAIN_MENU);
        return sendMessage;
    }

    private SendMessage sendEnterNameButtonMessage(Long chatId, Client client) {
        ReplyKeyboardMarkup sendName = ReplyKeyboardMarkup.builder()
                .resizeKeyboard(true)
                .keyboardRow(new KeyboardRow(List.of(
                        KeyboardButton.builder()
                                .text(client.getName().toUpperCase()).build()
                )))
                .keyboardRow(makeBackButton(client.getLanguage())).build();
        client.setState(StateEnum.ENTERING_NAME);

        return SendMessage.builder()
                .text(getMessageByLanguageAndKey("enterName", client.getLanguage()))
                .chatId(chatId)
                .replyMarkup(sendName)
                .build();
    }

    private String oneOrderToTextFormat(Order order) {
        //todo Buyurtma raqami: 9988540
        //Holat: Заказ передан клиенту
        //Manzil: Узбекистан, Ташкент, улица Беруни, 3А
        //
        //1️⃣ ✖️ Лаваш говяжий
        //
        //To'lov turi: Click
        //
        //Mahsulotlar: 23 000 so'm
        //Yetkazib berish: 10 000 so'm
        //Jami: 33 000 so'm
        return order.getStatus().name();
    }

    private static Optional<Restaurant> getBranchThatEqualsToText(String text, List<Restaurant> restaurants) {
        return restaurants.stream().filter(restaurant -> restaurant.title().equals(text)).findFirst();
    }

    private SendMessage sendNearestBranchesMessage(Long chatId, Client client, List<Restaurant> restaurants) {
        Iterator<Restaurant> iterator = restaurants.iterator();
        KeyboardRow nearestBranchRow = new KeyboardRow(List.of(
                KeyboardButton.builder()
                        .text(restaurants.get(0).title()).build()
        ));
        KeyboardRow branchRow = new KeyboardRow();
        while (iterator.hasNext()) {
            iterator.next();
            if (iterator.hasNext()) {
                branchRow.add(iterator.next().title());
            }
            if (iterator.hasNext()) {
                branchRow.add(iterator.next().title());
            }
        }
        ReplyKeyboardMarkup getBranches = ReplyKeyboardMarkup.builder()
                .keyboard(List.of(nearestBranchRow, branchRow))
                .keyboardRow(new KeyboardRow(List.of(
                        makeBackButton(client.getLanguage()).get(0),
                        KeyboardButton.builder().text(getMessageByLanguageAndKey("otherBranchesButton", client.getLanguage())).build()
                )))
                .resizeKeyboard(true)
                .build();
        return SendMessage.builder()
                .chatId(chatId)
                .replyMarkup(getBranches)
                .text(getMessageByLanguageAndKey("nearestBranchMessage", client.getLanguage())
                        .formatted(restaurants.get(0).title()))
                .build();
    }

    private SendMessage sendAllBranchesMessage(Long chatId, Client client) {
        List<Restaurant> restaurants = HelperService.getRestaurants(client.getRegion());
        Iterator<Restaurant> iterator = restaurants.iterator();
        List<KeyboardRow> keyboard = new ArrayList<>(restaurants.size() / 2);
        KeyboardRow row = new KeyboardRow();
        while (iterator.hasNext()) {
            row.add(iterator.next().title());
            if (iterator.hasNext())
                row.add(iterator.next().title());
            keyboard.add(row);
            row = new KeyboardRow();
        }
        ReplyKeyboardMarkup allBranchesButtons = ReplyKeyboardMarkup.builder()
                .keyboard(keyboard)
                .keyboardRow(makeBackButton(client.getLanguage()))
                .resizeKeyboard(true)
                .build();

        return SendMessage.builder()
                .chatId(chatId)
                .text(getMessageByLanguageAndKey("chooseBranch", client.getLanguage()))
                .replyMarkup(allBranchesButtons)
                .build();
    }


    private SendMessage makeSendMessageWithRegion(Long chatId, Client client) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        sendMessage.setText(getMessageByLanguageAndKey("chooseRegion", client.getLanguage()));
        sendMessage.setReplyMarkup(makeRegionButtons());
        return sendMessage;
    }


    private InlineKeyboardMarkup menuWithOrder(LanguageEnum language) {

        InlineKeyboardButton menu = new InlineKeyboardButton(getMessageByLanguageAndKey("menu", language));
        menu.setCallbackData(BotConstantText.MENU);
        List<InlineKeyboardButton> first = List.of(menu);

        InlineKeyboardButton aboutUs = new InlineKeyboardButton(getMessageByLanguageAndKey("aboutUsInlineButton", language));
        aboutUs.setCallbackData(BotConstantText.ABOUT_US_BUTTON);
        InlineKeyboardButton myOrders = new InlineKeyboardButton(getMessageByLanguageAndKey("myOrdersInlineButton", language));
        myOrders.setCallbackData(BotConstantText.MY_ORDERS_BUTTON);
        List<InlineKeyboardButton> secondRow = List.of(aboutUs, myOrders);

        InlineKeyboardButton branches = new InlineKeyboardButton(getMessageByLanguageAndKey("branches", language));
        branches.setCallbackData(BotConstantText.BRANCHES);
        List<InlineKeyboardButton> third = List.of(branches);

        InlineKeyboardButton feedback = new InlineKeyboardButton(getMessageByLanguageAndKey("feedback", language));
        feedback.setCallbackData(BotConstantText.FEEDBACK);
        InlineKeyboardButton settings = new InlineKeyboardButton(getMessageByLanguageAndKey("settings", language));
        settings.setCallbackData(BotConstantText.SETTINGS);
        List<InlineKeyboardButton> fourth = List.of(feedback, settings);

        InlineKeyboardButton newMenu = new InlineKeyboardButton(getMessageByLanguageAndKey("newMenu", language));
        menu.setCallbackData(BotConstantText.NEW_MENU);
        List<InlineKeyboardButton> fifth = List.of(newMenu);

        return new InlineKeyboardMarkup(List.of(first, secondRow, third, fourth, fifth));
    }

    private ReplyKeyboard makeConfirmLocationButtons(LanguageEnum language) {
        return ReplyKeyboardMarkup.builder()
                .resizeKeyboard(true)
                .keyboard(List.of(new KeyboardRow(
                        List.of(
                                KeyboardButton.builder()
                                        .text(getMessageByLanguageAndKey("noButton", language))
                                        .build(),
                                KeyboardButton.builder()
                                        .text(getMessageByLanguageAndKey("yesButton", language))
                                        .build()
                        )
                )))
                .build();
    }

    private ReplyKeyboardMarkup makeSendLocation(LanguageEnum language) {
        return ReplyKeyboardMarkup.builder()
                .resizeKeyboard(true)
                .keyboard(List.of(
                        new KeyboardRow(List.of(KeyboardButton.builder()
                                .text(getMessageByLanguageAndKey("sendGeoLocationButton", language))
                                .requestLocation(true)
                                .build())),
                        makeBackButton(language)
                ))
                .build();
    }

    private ReplyKeyboardMarkup makeOrderTypeButtons(LanguageEnum language) {
        KeyboardRow firstRow = new KeyboardRow(List.of(
                KeyboardButton.builder()
                        .text(getMessageByLanguageAndKey("deliveryOrderType", language))
                        .build(),
                KeyboardButton.builder()
                        .text(getMessageByLanguageAndKey("selfOrderType", language))
                        .build()
        ));

        return ReplyKeyboardMarkup.builder()
                .resizeKeyboard(true)
                .keyboard(List.of(firstRow, makeBackButton(language)))
                .build();
    }

    private KeyboardRow makeBackButton(LanguageEnum language) {
        return new KeyboardRow(List.of(
                KeyboardButton.builder()
                        .text(getMessageByLanguageAndKey("backButton", language))
                        .build()
        ));
    }

    private void addClient(Long chatId, String name) {
        HelperService.getClients().put(chatId,
                new Client(name, chatId)
        );
    }

    public static Client getClient(Long chatId) {
        return HelperService.getClients().get(chatId);
    }

    private String getMessageByLanguageAndKey(String key, LanguageEnum language) {
        ResourceBundle resourceBundle = HelperService.getResources().get(language);
        return resourceBundle.getString(key);
    }

    private ReplyKeyboardMarkup makeRegionButtons() {
        Iterator<Region> iterator = HelperService.getRegions().iterator();
        List<KeyboardRow> rows = new ArrayList<>();
        while (iterator.hasNext()) {
            KeyboardRow row = new KeyboardRow();
            row.add(new KeyboardButton(iterator.next().name()));
            if (iterator.hasNext())
                row.add(new KeyboardButton(iterator.next().name()));
            rows.add(row);
        }
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup(rows);
        replyKeyboardMarkup.setResizeKeyboard(true);
        return replyKeyboardMarkup;
    }

    private String getRandomCode(int size) {
        return String.valueOf(new Random().nextInt((int) Math.pow(10, size)));
    }

    private String makeSmsCodeMessage(Client client, String code) {
        String smsCodeMessage = getMessageByLanguageAndKey("smsCodeMessage", client.getLanguage());
        return String.format(smsCodeMessage, client.getPhone(), code);
    }

    private SendMessage makeSendContactSendMessage(Long chatId, LanguageEnum language, Client client) {
        client.resetAttemptCount();
        client.setState(StateEnum.SEND_CONTACT);
        return SendMessage.builder().chatId(chatId)
                .text(getMessageByLanguageAndKey("enterPhone", language))
                .replyMarkup(makeSendContactButton(language)).build();
    }

    private ReplyKeyboardMarkup makeSendContactButton(LanguageEnum language) {
        KeyboardButton contactButton = new KeyboardButton(getMessageByLanguageAndKey("sendMyNumber", language));
        contactButton.setRequestContact(true);
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup(List.of(new KeyboardRow(List.of(contactButton))));
        replyKeyboardMarkup.setResizeKeyboard(true);
        return replyKeyboardMarkup;
    }

    private InlineKeyboardMarkup makeMainMenuInline(LanguageEnum language) {
        List<InlineKeyboardButton> firstRow = List.of(InlineKeyboardButton.builder()
                .text(getMessageByLanguageAndKey("makeOrderInlineButton", language))
                .callbackData(BotConstantText.MAKE_ORDER_BUTTON)
                .build());

        List<InlineKeyboardButton> secondRow = List.of(
                InlineKeyboardButton.builder()
                        .text(getMessageByLanguageAndKey("aboutUsInlineButton", language))
                        .callbackData(BotConstantText.ABOUT_US_BUTTON)
                        .build(),
                InlineKeyboardButton.builder()
                        .text(getMessageByLanguageAndKey("myOrdersInlineButton", language))
                        .callbackData(BotConstantText.MY_ORDERS_BUTTON)
                        .build());

        List<InlineKeyboardButton> third = List.of(InlineKeyboardButton.builder()
                .text(getMessageByLanguageAndKey("branches", language))
                .callbackData(BotConstantText.BRANCHES)
                .build());

        List<InlineKeyboardButton> fourth = List.of(
                InlineKeyboardButton.builder()
                        .text(getMessageByLanguageAndKey("feedback", language))
                        .callbackData(BotConstantText.FEEDBACK)
                        .build(),
                InlineKeyboardButton.builder()
                        .text(getMessageByLanguageAndKey("settings", language))
                        .callbackData(BotConstantText.SETTINGS)
                        .build());

        return InlineKeyboardMarkup.builder()
                .keyboard(List.of(firstRow, secondRow, third, fourth))
                .build();
    }

    private InlineKeyboardMarkup rootCategoriesInline(LanguageEnum language) {
        List<Category> categories = getCategoriesByParent(null);
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<InlineKeyboardButton> row = new ArrayList<>(2);

        for (Category category : categories) {
            row.add(InlineKeyboardButton.builder()
                    .text((category.getEmoji() != null ? category.getEmoji() : "") + category.getName())
                    .callbackData("Category#" + category.getId())
                    .build());
            if (row.size() == 2) {
                rows.add(row);
                row = new ArrayList<>(2);
            }
        }
        if (row.size() == 1)
            rows.add(row);

        return InlineKeyboardMarkup.builder()
                .keyboard(rows)
                .build();
    }

    private InlineKeyboardMarkup makeMainInlineButton(LanguageEnum language) {
        InlineKeyboardButton mainMenu = new InlineKeyboardButton(getMessageByLanguageAndKey("mainMenuInlineButton", language));
        mainMenu.setCallbackData(BotConstantText.MAIN_MENU);
        List<InlineKeyboardButton> firstRow = List.of(mainMenu);
        return new InlineKeyboardMarkup(List.of(firstRow));
    }

    private SendMessage rootCategory(Long chatId) {
        Client client = getClient(chatId);
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        sendMessage.setText(getMessageByLanguageAndKey("foodMenuWithMenu", client.getLanguage()));
        sendMessage.setParseMode("HTML");
        sendMessage.setReplyMarkup(rootCategoriesInline(client.getLanguage()));
        client.setState(StateEnum.CHOOSE_CATEGORY_OR_PRODUCT);
        return sendMessage;
    }

    private ReplyKeyboardMarkup mainMenuButton() {
        //todo resource bundle
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup(
                List.of(new KeyboardRow(
                        List.of(new KeyboardButton(BotConstantText.MAIN_MENU))
                ))
        );
        replyKeyboardMarkup.setResizeKeyboard(true);
        return replyKeyboardMarkup;
    }

    private SendMessage mainMenu(Long chatId, String text) {
        Client client = getClient(chatId);
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        sendMessage.setText(text);
        sendMessage.setParseMode("HTML");
        sendMessage.setReplyMarkup(makeMainMenuInline(client.getLanguage()));
        client.setState(StateEnum.MAIN_MENU);
        return sendMessage;
    }

    private SendMessage mainMenuFromFoodMenu(Long chatId, String text) {
        Client client = getClient(chatId);
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);
        sendMessage.setText(text);
        sendMessage.setParseMode("HTML");
        sendMessage.setReplyMarkup(menuWithOrder(client.getLanguage()));
        return sendMessage;
    }

    private Order getLastOrder(Long chatId) {

        List<Order> orders = HelperService.getOrders().getOrDefault(chatId, new ArrayList<>());
        if (!orders.isEmpty() &&
                orders.get(orders.size() - 1)
                        .getStatus().equals(OrderStatus.DRAFT))
            return orders.get(orders.size() - 1);

        Order order = new Order();
        orders.add(order);
        HelperService.getOrders().put(chatId, orders);
        return order;
    }

    private List<Restaurant> nearestRestaurants(Client client, Order order) {
        return HelperService.getRestaurants(client.getRegion()).stream()
                .sorted((o1, o2) -> (int) (calculateDistance(order.getLat(), order.getLon(), o1.lat(), o1.lon()) -
                        calculateDistance(order.getLat(), order.getLon(), o2.lat(), o2.lon()))).limit(3).toList();
    }

    private double calculateDistance(double clientLat, double clientLon, double restaurantLat, double restaurantLon) {
        final double EARTH_RADIUS = 6.378;
        double dLat = Math.toRadians(clientLat - restaurantLat);
        double dLng = Math.toRadians(clientLon - restaurantLon);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(Math.toRadians(restaurantLat)) * Math.cos(Math.toRadians(clientLat)) *
                        Math.sin(dLng / 2) * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return EARTH_RADIUS * c;
    }

    private List<Category> getCategoriesByParent(Category parent) {
        return HelperService.getCategories().stream()
                .filter(category -> {
                    if (parent == null && category.getParent() == null)
                        return true;
                    if (parent != null && category.getParent() != null
                            && category.getParent().getId().equals(parent.getId()))
                        return true;
                    return false;
                })
                .collect(Collectors.toList());
    }

    private Optional<Category> getCategoryById(String parentId) {
        return HelperService.getCategories().stream()
                .filter(category -> category.getId().equals(parentId))
                .findFirst();
    }

    public List<SendPhoto> sendPhotoInitials() {
        try {
            List<SendPhoto> sendPhotos = new ArrayList<>();
            File category = new File("D:\\LESSON\\g28\\05\\app-oqtepa-lavash-bot\\photos\\category");
            for (File file : category.listFiles()) {
                sendPhotos.add(SendPhoto.builder()
                        .chatId(-994632275L)
                        .photo(new InputFile(new FileInputStream(file), file.getName()))
                        .build());
            }
            File products = new File("D:\\LESSON\\g28\\05\\app-oqtepa-lavash-bot\\photos\\product");

            for (File file : products.listFiles()) {
                sendPhotos.add(SendPhoto.builder()
                        .chatId(-994632275L)
                        .photo(new InputFile(new FileInputStream(file), file.getName()))
                        .build());
            }
            return sendPhotos;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private List<Product> getProductsByCategory(String categoryId) {
        return HelperService.getProducts().values().stream()
                .filter(product -> product.getCategory().getId().equals(categoryId))
                .collect(Collectors.toList());
    }
}
