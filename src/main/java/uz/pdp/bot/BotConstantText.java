package uz.pdp.bot;

public interface BotConstantText {

    String UZ = "\uD83C\uDDFA\uD83C\uDDFF O'zbekcha";
    String RU = "\uD83C\uDDF7\uD83C\uDDFA Русский";
    String EN = "\uD83C\uDDFA\uD83C\uDDF8 English";
    String MAIN_MENU = "⬅️ Main menu";
    String MAKE_ORDER_BUTTON = "make_order_button";
    String ABOUT_US_BUTTON = "about_us_button";
    String MY_ORDERS_BUTTON = "my_orders_button";
    String BRANCHES = "branches";
    String FEEDBACK = "feedback";
    String SETTINGS = "settings";
    String CHICKEN = "chicken";

    String SALADS = "salads";
    String DRINKS = "drinks";
    String LAVASH = "lavash";
    String SETS = "sets";
    String PIZZA = "pizza";
    String HOT_DOG = "hot-dog";

    String HAGGI = "haggi";
    String ClUB_SANDWICH = "club sandwich";

    String SNAKS = "snaks";
    String MENU = "menu";
    String NEW_MENU = "new Order";
    String LEAVE_FEEDBACK = "leave_feedback";
    String ADD_BASKET = "add_basket";
    String BACK = "back";
}
