package uz.pdp.bot;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import uz.pdp.enums.LanguageEnum;
import uz.pdp.model.*;

import java.io.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

public class HelperService {

    @Getter
    private static final Map<Long, Client> clients;

    @Getter
    private static final List<Region> regions = List.of(
            new Region("Toshkent"),
            new Region("Samarqand"),
            new Region("Andijon"),
            new Region("Farg'ona"),
            new Region("Namangan"));

    @Getter
    private static final List<Category> categories;

    @Getter
    private static final Map<String,Product> products = new HashMap<>();

    @Getter
    private static final Map<LanguageEnum, ResourceBundle> resources = new HashMap<>() {{
        put(LanguageEnum.UZ, ResourceBundle.getBundle("messages", new Locale("UZ")));
        put(LanguageEnum.RU, ResourceBundle.getBundle("messages", new Locale("RU")));
        put(LanguageEnum.EN, ResourceBundle.getBundle("messages", new Locale("EN")));
    }};


    private static final Map<Region, List<Restaurant>> restaurants;
    @Getter
    private static final Map<Long, List<Order>> orders;
    private static final ObjectMapper mapper;

    static {
        beforeShutDown();
        mapper = new ObjectMapper();
        Map<Long, Client> temp;
        try {
            ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream("clients.txt"));
            temp = (Map<Long, Client>) inputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            temp = new ConcurrentHashMap<>();
        }

        clients = temp;

        Map<Long, List<Order>> orderTemp;
        try {
            ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream("orders.txt"));
            orderTemp = (Map<Long, List<Order>>) inputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            orderTemp = new ConcurrentHashMap<>();
        }
        List<Category> categoryList;
        try {
            ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream("categories.txt"));
            categoryList = (List<Category>) inputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            categoryList = new CopyOnWriteArrayList<>();
        }
        categories = categoryList;

        orders = orderTemp;
        List<Restaurant> tempRes;
        try {
            tempRes = mapper.readValue(new File("restaurants.json"), new TypeReference<>() {
            });
        } catch (IOException e) {
            tempRes = new ArrayList<>();
        }
        restaurants = tempRes.stream().collect(Collectors.groupingBy(Restaurant::region));
    }

    static {

        Category chicken = new Category("🍗", "Chicken");
        Category salads = new Category("🥗", "Salads");
        Category drinks = new Category("🥤", "Drinks");
        Category sauces = new Category("🍅", "Sauces");
        Category hotDrinks = new Category("Hot drinks", drinks);
        Category coldDrinks = new Category("Cold drinks", drinks);
        Category pepsi = new Category("Pepsi", coldDrinks);
        if (categories.isEmpty())
            Collections.addAll(categories,
                    chicken,
                    salads,
                    drinks,
                    sauces,
                    hotDrinks,
                    coldDrinks,
                    pepsi);


        Product Jo= new Product("Jo'ja box", 20000, chicken, "Zur juja");
        products.put(Jo.getId(),Jo);
        Product Stips= new Product("Stips 5 dona", 22000, chicken, "Zur juja");
        products.put(Stips.getId(),Stips);
        Product Mujskoy= new Product("Mujskoy kapriz", 30000, salads, "Zur juja");
        products.put(Mujskoy.getId(),Mujskoy);
        Product Sezar= new Product("Sezar", 15000, salads, "Zur juja");
        products.put(Sezar.getId(),Sezar);
        Product Ketchup= new Product("Ketchup", 3000, sauces, "Zur juja");
        products.put(Ketchup.getId(),Ketchup);
        Product Cheese= new Product("Cheese sauce", 5000, sauces, "Zur juja");
        products.put(Cheese.getId(),Cheese);
        Product Black= new Product("Black tea", 3000, hotDrinks, "Zur juja");
        products.put(Black.getId(),Black);
        Product Green= new Product("Green tea", 3000, hotDrinks, "Zur juja");
        products.put(Green.getId(),Green);
        Product Lemon= new Product("Lemon tea", 5000, hotDrinks, "Zur juja");
        products.put(Lemon.getId(),Lemon);
        Product Sochnaya= new Product("Sochnaya dolina", 13000, coldDrinks, "Zur juja");
        products.put(Sochnaya.getId(),Sochnaya);
        Product Lipton= new Product("Lipton 0.5", 8000, coldDrinks, "Zur juja");
        products.put(Lipton.getId(),Lipton);
        Product Pepsi15= new Product("Pepsi 1.5", 15000, pepsi, "Zur juja");
        products.put(Pepsi15.getId(),Pepsi15);
        Product Pepsi04= new Product("Pepsi 0.4", 8000, pepsi, "Zur juja");
        products.put(Pepsi04.getId(),Pepsi04);
        Product Pepsi03= new Product("Pepsi 0.3", 7000, pepsi, "Zur juja");
        products.put(Pepsi03.getId(),Pepsi03);
        Product Pepsi05= new Product("Pepsi 0.5", 10000, pepsi, "Zur juja");
        products.put(Pepsi05.getId(),Pepsi05);


//                new Category("🌯", "Lavash"),
//                new Category("🍟🌯🥤", "Sets"),
//                new Category("🍕", "Pizza"),
//                new Category("🍔", "Burgers and Doners"),
//                new Category("🌭", "Hot-Dog"),
//                new Category("🥙", "Haggi"),
//                new Category("🥪", "Club Sandwich"),
//                new Category("🍟", "Snaks"),
    }

    private static void beforeShutDown() {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream("clients.txt"));
                outputStream.writeObject(clients);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            try {
                ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream("orders.txt"));
                outputStream.writeObject(orders);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            try {
                ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream("categories.txt"));
                outputStream.writeObject(categories);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }));
    }

    public static List<Restaurant> getRestaurants(Region region) {
        return restaurants.getOrDefault(region, Collections.emptyList());
    }
}
