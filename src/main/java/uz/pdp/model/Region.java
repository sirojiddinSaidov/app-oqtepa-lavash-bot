package uz.pdp.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.io.Serializable;

public record Region(String name) implements Serializable {
}

