package uz.pdp.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
public class Product {


    private String id=UUID.randomUUID().toString();

    private String name;

    private double price;

    private Category category;

    private String description;

    private String photo;

    public Product(String name, double price, Category category, String description) {
        this.name = name;
        this.price = price;
        this.category = category;
        this.description = description;

    }
}
