package uz.pdp.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import uz.pdp.enums.LanguageEnum;
import uz.pdp.enums.StateEnum;

import java.io.Serializable;

@Getter
@AllArgsConstructor
@Setter
@Builder
public class Client implements Serializable {
    private String name;

    private long chatId;

    private LanguageEnum language;

    private String furtherLocationDetails;

    private StateEnum state;

    private String phone;

    private boolean verified;

    private Region region;

    private String SMSCode;

    private int attemptCount = 3;

    public Client(long chatId) {
        this.chatId = chatId;
    }

    public Client(String name, long chatId) {
        this.name = name;
        this.chatId = chatId;
    }

    public void decrementAttemptCount() {
        attemptCount--;
    }

    public void resetAttemptCount() {
        attemptCount = 3;
    }
}
