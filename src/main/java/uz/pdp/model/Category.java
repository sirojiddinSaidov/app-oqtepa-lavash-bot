package uz.pdp.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class Category implements Serializable {
    private static int counter;

    private String id;

    private String emoji;

    private String name;

    private Category parent;

    private String photo;

    public Category(String emoji, String name) {
        this.emoji = emoji;
        this.name = name;
    }

    public Category(String name, Category parent) {
        this.name = name;
        this.parent = parent;
    }

    {
        id = "" + (++counter);
    }
}
