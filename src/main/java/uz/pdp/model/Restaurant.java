package uz.pdp.model;

import java.io.Serializable;

public record Restaurant(String title, String fullAddress, Region region, double lon, double lat) implements Serializable {
}
