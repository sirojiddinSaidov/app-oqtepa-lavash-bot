package uz.pdp.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.pdp.enums.OrderStatus;
import uz.pdp.enums.OrderTypeEnum;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Order implements Serializable {

    private Client client;

    private OrderTypeEnum orderType;

    private OrderStatus status = OrderStatus.DRAFT;

    private Double lat;

    private Double lon;
    private Restaurant branch;
}
